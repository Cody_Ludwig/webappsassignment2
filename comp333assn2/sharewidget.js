/*
 * Constructor function for a ShareWidget instance.
 * 
 * container_element : a DOM element inside which the widget will place its UI
 *
 */
 
function ShareWidget(container_element){
	
	//declare variables for the data properties of the widget
	
	var publicShareData = new Array();
	
	var _ui = {     //an object literal representing the widget's UI
		
		
		titleContainer : null,
		widgetTitle : null,

		container : null,
		titlebar : null,

		listContainer : null,
		listContents : null,

		companiesContainer : null,
		compainesContents : null,
		
		
		
		
		//declare variables for the UI properties of the widget
	};
	
	/**
	* Constructor function for an inner object to hold the full share data for a company
	*/

	var ShareLine = function(s_company, s_price, s_change){
		
		//declare variables for the data properties of one company's share information
	
		var _ui = {					//an object literal representing the UI for the share info
			
			//declare variables for the UI properties of the ShareLine
			sharesContainer : null,
			sharesSpace : null,
			textSpace : null,
			shareName : null,
			sharePrice : null,
			shareChange : null,
			textSpaceContainer : null,
		};
		
		/* function to create the DOM elements needed for the ShareLine UI
		*/
		var _createUI = function(){
				
			//create and initialise each of the UI elements and add them to the
			//_ui object
			
			//_ui.textSpaceContainer = container_element;
			

			
			//_ui.textSpaceContainer = document.createElement("div");
			//_ui.textSpaceContainer.setAttribute("ID", container_element.id+"container");
	
			_ui.sharesContainer = document.createElement("div");
			//_ui.textSpaceContainer.appendChild(_ui.sharesContainer);
			
			_ui.shareName = document.createTextNode(s_company);
			_ui.sharePrice = document.createTextNode(s_price);
			_ui.shareChange = document.createTextNode(s_change);

				
				
			_ui.sharesContainer.setAttribute("style", "background-color: #42a7f4");
			

						
			
		};
		this.displayAppend = function(){
		
			//textSpaceContainer = document.getElementById(container_element.id+"container");
			_ui.shareName = document.createTextNode(s_company);
			_ui.sharePrice = document.createTextNode(s_price);
			_ui.shareChange = document.createTextNode(s_change);
			
			_ui.sharesContainer.appendChild(_ui.shareName);
			_ui.sharesContainer.appendChild(_ui.sharePrice);
			_ui.sharesContainer.appendChild(_ui.shareChange);
							
	
			}
		

	//getter methods for ShareLine should be defined next as needed 

	
		_createUI();    //call this function last to build the UI
	 
  };  //End of ShareLine constructor function
	
	
	//getter methods for ShareWidget should be defined next as needed 

	
	//private method to construct the DOM subtree for the UI and put into container element
	var _createUI = function(){
		alert("createUI");
		//create the DOM elements needed for the widget and add to the _ui var
		
		_ui.titleContainer = container_element;
		_ui.widgetTitle = document.createElement("div");
		_ui.titleContainer.appendChild(_ui.widgetTitle);

		_ui.widgetTitle.setAttribute("style", "background-color: #42a7f4");
		_ui.widgetTitle.label = document.createTextNode("Share Price Widget");
		_ui.widgetTitle.appendChild(_ui.widgetTitle.label);
		
		
		_ui.container = container_element;
		_ui.container.setAttribute("style", "width: 30%");

		_ui.titlebar = document.createElement("div");
		_ui.container.appendChild(_ui.titlebar);
		_ui.titlebar.textNextToSelector = document.createTextNode("Select a company:");

		_ui.titlebar.setAttribute("style", "background-color: #42a7f4");
		_ui.titlebar.selection = document.createElement("select");
		

		_ui.listContainer = container_element;
		_ui.listContents  = document.createElement("div");
		_ui.listContents.setAttribute("style", "background-color: #FFFFFF");
		_ui.listContainer.appendChild(_ui.listContents);
		_ui.listContents.radioTitle = document.createTextNode("Sort by");
		_ui.listContents.radioLabel1 = document.createTextNode("Name");
		

		_ui.listContents.radio1 = document.createElement("input");
		_ui.listContents.radio1.setAttribute("type", "radio");
		_ui.listContents.radio1.setAttribute("value", "nameButton");
		_ui.listContents.radio1.setAttribute("name", "radioButton");

		_ui.listContents.radioLabel2 = document.createTextNode("Price");
		

		_ui.listContents.radio2 = document.createElement("input");
		_ui.listContents.radio2.setAttribute("type", "radio");
		_ui.listContents.radio2.setAttribute("value", "priceButton");
		_ui.listContents.radio1.setAttribute("name", "radioButton");

		_ui.listContents.appendChild(_ui.listContents.radioTitle);
		_ui.listContents.appendChild(_ui.listContents.radioLabel1);
		_ui.listContents.appendChild(_ui.listContents.radio1);
		_ui.listContents.appendChild(_ui.listContents.radioLabel2);
		_ui.listContents.appendChild(_ui.listContents.radio2);

		//_ui.listContainer.appendChild(_ui.listContents);
		//options for the selector
		_ui.titlebar.option1 = document.createElement("OPTION");
		_ui.titlebar.option1.setAttribute("value","NZ Windfarms");
		_ui.titlebar.option1text = document.createTextNode("NZ Wind Farms");
		_ui.titlebar.option1.appendChild(_ui.titlebar.option1text);
		
		_ui.titlebar.option2 = document.createElement("OPTION");
		_ui.titlebar.option2.setAttribute("value","Foley Wines");
		_ui.titlebar.option2text = document.createTextNode("Foley Wines");
		_ui.titlebar.option2.appendChild(_ui.titlebar.option2text);
		
		_ui.titlebar.option3 = document.createElement("OPTION");
		_ui.titlebar.option3.setAttribute("value","Geneva Finance");
		_ui.titlebar.option3text = document.createTextNode("Geneva Finance");
		_ui.titlebar.option3.appendChild(_ui.titlebar.option3text);
		
		_ui.titlebar.option4 = document.createElement("OPTION");
		_ui.titlebar.option4.setAttribute("value","Xero Live");
		_ui.titlebar.option4text = document.createTextNode("Xero Live");
		_ui.titlebar.option4.appendChild(_ui.titlebar.option4text);
		
		_ui.titlebar.option5 = document.createElement("OPTION");
		_ui.titlebar.option5.setAttribute("value","Moa Group Ltd");
		_ui.titlebar.option5text = document.createTextNode("Moa Group Ltd");
		_ui.titlebar.option5.appendChild(_ui.titlebar.option5text);
		
		_ui.titlebar.option6 = document.createElement("OPTION");
		_ui.titlebar.option6.setAttribute("value","Solution Dynamics");
		_ui.titlebar.option6text = document.createTextNode("Solution Dynamics");
        _ui.titlebar.option6.appendChild(_ui.titlebar.option6text);

		//appending the text and the selection to the titlebar
		_ui.titlebar.appendChild(_ui.titlebar.textNextToSelector);
		_ui.titlebar.appendChild(_ui.titlebar.selection);

		_ui.titlebar.selection.appendChild(_ui.titlebar.option1);
		_ui.titlebar.selection.appendChild(_ui.titlebar.option2);
		_ui.titlebar.selection.appendChild(_ui.titlebar.option3);
		_ui.titlebar.selection.appendChild(_ui.titlebar.option4);
		_ui.titlebar.selection.appendChild(_ui.titlebar.option5);
		_ui.titlebar.selection.appendChild(_ui.titlebar.option6);


		
		alert("before request");

		_ui.titlebar.selection.onchange = function(){
			alert("Selection Made");
			retrieveShareInfo(_ui.titlebar.selection.value);
		};

	}

	
	/**
	 * private methods for the rest of the functionality should
	 * be added below 
	 */

	function ajaxRequest(method, url, async, data, callback){

		var request = new XMLHttpRequest();
		request.open(method,url,async);
		
		if(method == "POST"){
			request.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		}
		
		request.onreadystatechange = function(){
			if (request.readyState == 4) {
				if (request.status == 200) {
					var response = request.responseText;
					callback(response);
				} else {
					
				}
			}
		}
		
		request.send(data);
	}
	//Need to manipulate this..
	function retrieveShareInfo(selectionData) {
		alert("Requestb4");
		alert(selectionData);
		var url = "getShares.php";
		var data = "selectionData="+selectionData;
		ajaxRequest("POST", url, true, data, retrieveShareInfoCallback);
		alert("Requestafter");
	}

	function retrieveShareInfoCallback(response){
		alert("callback");
		var returnedData = JSON.parse(response);
		alert(returnedData);
		
		publicShareData.push(returnedData);
		refreshSharesDisplay();
		
	}
	function refreshSharesDisplay(){
		alert("refreshSharesDisplay");

		// while (textSpaceContainer.hasChildNodes()){
			// textSpaceContainer.removeChild(textSpaceContainer.firstChild);
		// }
		
		for(var i = 0; i < publicShareData.length; i++) {
			var business = publicShareData[i][0].businessName;
			var price = publicShareData[i][0].sharePrice;
			var change = publicShareData[i][0].shareChange;
		
			console.log(business, price, change);
			share=new ShareLine(business, price, change);
			share.displayAppend();
			console.log(publicShareData);
			
		}
		
		
	}
	 
	 
	 
	 
	 
	 /**
	  * private method to intialise the widget's UI on start up
	  */
	  var _initialise = function(container_element){
		  _createUI(container_element);
		  ShareLine(container_element);
		alert("Initialise");
	  	}
	  	
	  _initialise(container_element);   //finally call the _initialise function 
}
	 


	 